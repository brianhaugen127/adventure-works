from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Success"}

def test_read_item():
	response = client.get("/catalogue/1")
	assert response.status_code == 200
	assert response.json() == {
		"ProductID": 1,
		"Name": "Adjustable Race",
		"FinishedGoodsFlag": False,
		"ListPrice": "0.00"
	}

def test_read_items():
	response = client.get("/catalogue/")
	assert response.status_code == 200

def test_bad_url():
	response = client.get("/bad_url")
	assert response.status_code == 404
	assert response.json() == {"detail":"Not Found"}

def test_bad_item():
	response = client.get("/catalogue/noninteger_id")
	assert response.status_code == 422