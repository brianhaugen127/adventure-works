import React from "react";

function Item(props) {
  return (
  	<tr className="item">
  		<td>{props.name}</td>
  		<td>&nbsp;&nbsp;${props.price}&nbsp;&nbsp;</td>
  	</tr>
  	);
}

export default Item;