import React from "react";
import Item from "./Item";

function ItemList(props) {
  return (
    <table>
    	<tr> 
    		<td> <strong>Item</strong> </td>
    		<td> <strong>Price</strong> </td>
    	</tr>
    	{props.items.map(c => <Item name={c.name} price={c.price}/>)}
    </table>
  );
}

export default ItemList;