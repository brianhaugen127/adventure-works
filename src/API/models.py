from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship


from .database import Base
from .database import engine


class Item(Base):

    #Load the table of products from the SQL database
    __tablename__ = 'Product'
    __table_args__ = {
        'schema':'AdventureWorks2019.Production',
        'autoload':True,
        'autoload_with':engine
        }