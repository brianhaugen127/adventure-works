from sqlalchemy.orm import Session

from . import models, schemas

def get_item(db: Session, ProductID: int):
	return db.query(models.Item).filter(models.Item.ProductID == ProductID).first()

def get_items(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Item).filter(models.Item.FinishedGoodsFlag == 1).order_by(models.Item.ListPrice).offset(skip).limit(limit).all()
