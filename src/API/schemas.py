from typing import List, Optional

from pydantic import BaseModel, validator


class Item(BaseModel):

    ProductID: int
    Name: str
    FinishedGoodsFlag: bool
    ListPrice: Optional[float] = None 

    @validator('ListPrice')
    def correct_price_precision(cls, v):
    	return format(v, '.2f')

    class Config:
        orm_mode = True


