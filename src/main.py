from typing import List

from fastapi import Depends, FastAPI, HTTPException

from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session

from API import models, schemas, crud
from API.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


origins = [
	"http://localhost:3000",
	"localhost:3000"
]



app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
   allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.get("/", tags=["root"])
async def read_root() -> dict:
	return {"msg": "Success"}
			

@app.get("/catalogue/", response_model=List[schemas.Item])
def read_items(skip: int = 0, limit = None, db: Session = Depends(get_db)):
	items = crud.get_items(db, skip=skip, limit=limit)
	return items

@app.get("/catalogue/{ProductID}", response_model=schemas.Item)
def read_item(ProductID: int, db: Session = Depends(get_db)):
    item = crud.get_item(db, ProductID)
    return item