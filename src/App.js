import logo from './logo.svg';
import './App.css';
import axios from "axios";
import React, {Component} from "react";
import ItemList from "./components/ItemList.js";

class App extends Component {

	state = {
		items: []
	};

  	componentDidMount() {
    axios
    	.get("http://127.0.0.1:8000/catalogue/")
    	.then(response => {

    		// pull from API, process only relevant data 
    		const newItems = response.data.map(c => {
    			return {
    				name: c.Name,
    				price: c.ListPrice
    			};
    		});

        	
	    	const newState = Object.assign({}, this.state, {
	    		items: newItems
	    	});

    		// store the new state object in the component's state
    		this.setState(newState);

    	})
      .catch(error => console.log(error));
  	}



	render() {
	  return (
	    <div className="App">
	        <p>
	          Adventure Works
	        </p>
	     z <ItemList items={this.state.items} />
	    </div>
	  );
	}
}

export default App;
